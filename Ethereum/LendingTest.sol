//
//
// Сделать на эфире или графене простой смарт-контракт кредитной заявки.
//
// 1. Один юзер кидает заявку (можно через консоль): размер, процент, кол-во мес
// 2. Второй юзер может принять ее.
// 3. После этого кредит сначала переходит от кредитора к заёмщику. А потом равными кусками, с учётом %, возвращает их кредитору.
//
// Можно упростить до предела :)
//
//

contract KarmaArbitrator {
    // Стоимость активов кредитора
    // [на самом деле надо конечно прочитать баланс]
    uint public lenderWorth = 1000000;
    // Рассматриваем случай сделки без обеспечения, по доверию через ранг заемщика
    // минимальный ранг заемщика
    uint public borrowerLowRank = 5;

    // Ранг (карма:) текущего заемщика. [для теста меняем ручками] 
    uint public borrowerRank = 10;
}

contract LendingTest is KarmaArbitrator {
    
    uint loanVal;  // запрашиваемый кредит 
    uint loanTime; // время кредитования в месяцах
    uint fee;      // стоимость кредита целых % годовых
    uint payments = 0 ; 
    //
    // Баланс кредита
    //
    mapping (address => uint) balances;

    bool balanceIsFed = false; // баланс пополнен
    bool isLended = false;     // кредит получен
    
    address borrower;          // заемщик
    address lender;            // кредитор 
    
    
    //
    // Запуск контракта
    //
    
    // Предполагаем, что собственник контракта является заемщиком
    // 1. Выбрать адрес заемщика (https://ethereum.github.io/browser-solidity)
    // 2. Создать контракт 
    // 3. Установить параметры заема: объем, время, проценты
    function setBorrowerCard(uint value, uint time, uint f) public {
        borrower = msg.sender;
        loanVal = value;
        loanTime = time;
        fee = f;
        require(lender != borrower);
    }
    
    // Предполагаем, что собственник контракта является заемщиком
    // 1. Выбрать адрес (https://ethereum.github.io/browser-solidity)
    // 2. Установть кредитором текущий адрес
    function setLenderCard() public {
        lender = msg.sender;
        require(lender != borrower);
    }


    //
    // Утилиты
    //

    function getBorrowerCard() public constant returns (address) {
        return borrower;
    }
    
    function getLenderCard() public constant returns (address) {
        return lender;
    }
    
    function lendedBalance() public constant returns (uint balance) {
        return balances[lender];
    }
    
     function borrowerBalance() public constant returns (uint balance) {
        return balances[borrower];
    }
    
    //
    // Пополнить кредитный баланс кредитром
    //
    function upTheBalance() public returns (bool success) {
        require(msg.sender == lender);
        require(balanceIsFed == false);
        if (lenderWorth >= loanVal) {
            balances[lender] += loanVal;
            balanceIsFed = true;
            lenderWorth -= loanVal;
            Transfer(lender, lender, loanVal);
        }
    }
    
    //
    // Одобрить кредит и пополнить счет заемщика
    //
    function approveAndTransfer() public returns (bool success) {

        require(msg.sender == lender);
        require(isLended == false);
        require(borrowerRank>=borrowerLowRank);

        if(balances[lender] >= loanVal && balances[borrower] + loanVal >= balances[borrower]) {
            balances[lender] -= loanVal; 
            balances[borrower] += loanVal;
            
            isLended = true;
            
            Approval(lender, borrower, loanVal);
            Transfer(lender, borrower, loanVal);
            return true;
        } 
        return false;
    }
    
    //
    // Произвести погашение кредита. Тут, к стати, для меня есть не совсем понятный момент:
    // Эфир не позволяет исполнять контракты по расписанию. Получается, что кредитор (?) должен сам 
    // переодически лаунчить метод
    //
    function repayTheLoan() {
        require(msg.sender == lender);
        require(isLended == true);
        require(payments<loanTime);
        payments += 1;
        uint partFee = loanVal/loanTime * (100 + fee)/100;
        balances[lender] += partFee; 
        balances[borrower] -= partFee;
    }
    
    //
    // Вывести средства на личный счет с кредитного баланса 
    //
    function withdrawFromBalance() {
        require(msg.sender == lender);
        require(isLended == true);
        lenderWorth += balances[lender];
        balances[lender] = 0;
    }
    
    
    event Transfer(address indexed from, address indexed to, uint value);
    event Approval(address indexed owner, address indexed spender, uint value);
}